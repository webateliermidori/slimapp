<?php
/**
 * [基本ルータ]の書式は、一般的なMVCのルーティングが行えるもの
 * カテゴリを追加するには、コントローラを増やせばOK。
 * アクション名は自動的に決まる方式。
 *
 * テンプレートエンジンは Twig を使用し、/views フォルダに
 * コントローラ（カテゴリ）ごとにビューファイルを作成する
 * MarkDown が使える拡張機能が入っているので文書を高速で作成できる
 * {% markdown %}{% endmarkdown %} の間に書くこと
 *
 * ---
 * @TODO
 * SNSボタンを配置する
 * SASSで管理する
 * アナリティクスを入れる
 * キャッシュ処理を入れる
 * サイトごとにSP用の調整を入れる
 * カテゴリ、またはページごとにバナーを入れる
 * サイトマップ作成スクリプトを作成する
 * OGP の処理を入れる
 * RSS 配信の処理を入れる
 */
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config/define.php';

use Aptoma\Twig\Extension\MarkdownExtension;
use Aptoma\Twig\Extension\MarkdownEngine;

// マイクロフレームワークのインスタンス作成
$app = new \Slim\App();

// フレームワークのDIコンテナを設定
$container = $app->getContainer();
// ビューに Twig を使う
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig('./views');
    $basePath = rtrim(str_ireplace('index.php', '', $c['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($c['router'], $basePath));
    $engine = new MarkdownEngine\MichelfMarkdownEngine();
    $view->addExtension(new MarkdownExtension($engine));
    return $view;
};

/**
 * 基本ルータ
 */
$app->get('/', function ($request, $response, $args) {
    $data = [];
    $action = isset($args['id']) ? $args['id'] : 'index';
    return $this->view->render($response, '/index/index.html', $data);
});
$app->get('/company[/{id}]', function ($request, $response, $args) {
    $data = [
        "title" => "company",
        "description" => "company_description",
    ];
    $action = isset($args['id']) ? $args['id'] : 'index';
    return $this->view->render($response, '/company/'.$action.'.html', $data);
});

$app->run();