# SlimApp

## 特徴
* Slim フレームワークで軽量かつ柔軟なルーティング
* MarkDown で記述できるテンプレートエンジン(Twig)
* サイト名、サイト説明などの基本設定が構築済み
* Bootstrap3 をパッケージ済

### 検証中
* サイト内リンクの検証
* ルーティングのサブルーティング

### 実装予定
* pure.css, jQuery代替に移行する
* Bower 重いのでやめる
* OGP の処理を入れる
* SNSボタンを配置する
* RSS 配信の処理を入れる
* SASSで管理する
* アナリティクスを入れる
* キャッシュ処理を入れる
* カテゴリ、またはページごとにバナーを入れる
* サイトマップ作成スクリプト
* 静的コンテンツ出力スクリプト

## インストール
composer install を実行

Bower で Bootstrap3 を導入済み

Bower を使って導入したものは asset ルート直下に配置されるが、Bootstrap3　の font フォルダは　asset 直下に移動するなどの対応が必要。


## 初期設定
config/define.php を編集する
* タイムゾーン
* サイト名
* サイト説明
* サイトサブ説明


## 更新作業
/index.php にカテゴリを増やしたら、/views 以下にカテゴリフォルダを作成し、アクション名に合わせて twig ファイルを設置する
例) /company/ -> views/company/index.html

/md フォルダに　Markdown 形式で記述して、 viewHTML でソースを参照して貼りつけるなどする

css や　js を追加したら、キャッシュ処理を追加するのを忘れないこと
  ファイルの更新日付を取得して、define したものを追加すれば良さそう

## 課題
サイト内リンクが未検証

静的ファイルを作成する？
http://hayashikejinan.com/webwork/php/492/