<?php
/**
 * サイトの基本情報を設定する
 */
date_default_timezone_set('Asia/Tokyo');
$thisYear = new DateTime();

// サイト基本情報（サイト名、説明）
define("SITE_NAME",         "hoge"); // {{ constant("SITE_NAME") }}
define("SITE_DESCRIPTION",  "hoge_description"); // {{ constant("SITE_DESCRIPTION") }}
define("SITE_SUB_TEXT",  "site_sub_text"); // {{ constant("SITE_SUB_TEXT") }}

// コピーライトに表記する年号（今年yyyy）
define("SITE_UPDATE_YEAR",  $thisYear->format('Y')); // {{ constant("SITE_UPDATE_YEAR") }}

// キャッシュ（時間と分まで記述すること）
define("CACHE_TIME",  '201703081440');  // ?date={{ constant("CACHE_TIME") }}
